#include "stdafx.h"
#include <columns_ui-sdk/ui_extension.h>
#include <d2d1_3.h>
#include <list>
#include <vector>

DECLARE_COMPONENT_VERSION("foo_cui_spectrogram",
"1.0.0",
"compiled: " __DATE__ "\n"
"with Panel API version: " UI_EXTENSION_VERSION
);

VALIDATE_COMPONENT_FILENAME("foo_cui_spectrogram.dll");

class SpectrogramWindow : public uie::container_ui_extension, play_callback_impl_base
{
public:
	SpectrogramWindow();
	~SpectrogramWindow();

	GUID const& get_extension_guid() const override;
	void get_name(pfc::string_base& out) const override;
	void get_category(pfc::string_base& out) const override;
	unsigned get_type() const override;

	void on_playback_starting(play_control::t_track_command p_command, bool p_paused) override;
	void on_playback_new_track(metadb_handle_ptr p_track) override;
	void on_playback_stop(play_control::t_stop_reason p_reason) override;
	void on_playback_pause(bool p_state) override;


private:
	LRESULT on_message(HWND wnd, UINT msg, WPARAM wp, LPARAM lp) override;
	class_data& get_class_data() const override;
	void get_menu_items(uie::menu_hook_t& p_hook) override;

	static LRESULT CALLBACK onChildMessage(HWND wnd, UINT msg, WPARAM wp, LPARAM lp);

	HRESULT createDeviceResourcesD2D();
	HRESULT renderD2D();
	void resizeD2D(UINT width, UINT height) const;
	void discardDeviceResourcesD2D();

	static void startVisualisation();
	static void stopVisualisation();
	void onVisualisation(audio_chunk const& chunk);

	static void SHARED_API visualisationTimerProc(HWND wnd, UINT msg, UINT event, DWORD time);
	static double lerp(double d1, double d2, double factor);

private:
	static const GUID _guid;
	static UINT_PTR _timerPtr;
	static std::list<SpectrogramWindow*> _instances;
	static service_ptr_t<visualisation_stream> _visualisationStream;
	static ID2D1Factory* _direct2dFactory;

	HWND _wnd;
	ID2D1HwndRenderTarget* _renderTarget;
	std::list<ID2D1Bitmap*> _lines;
};

void SpectrogramWindow::get_menu_items(uie::menu_hook_t&)
{
}

SpectrogramWindow::SpectrogramWindow() : play_callback_impl_base(play_callback::flag_on_playback_all), _wnd(nullptr), _renderTarget(nullptr)
{
	_instances.push_back(this);
	if (_direct2dFactory == nullptr)
		D2D1CreateFactory(D2D1_FACTORY_TYPE_MULTI_THREADED, &_direct2dFactory);
}

SpectrogramWindow::~SpectrogramWindow()
{
	_instances.remove(this);
}

HRESULT SpectrogramWindow::createDeviceResourcesD2D()
{
	HRESULT hr = S_OK;

	if (this->_renderTarget == nullptr)
	{
		RECT rc;
		GetClientRect(this->_wnd, &rc);

		D2D1_SIZE_U const size = D2D1::SizeU(
			rc.right - rc.left,
			rc.bottom - rc.top
		);

		hr = _direct2dFactory->CreateHwndRenderTarget(
			D2D1::RenderTargetProperties(),
			D2D1::HwndRenderTargetProperties(this->_wnd, size),
			&this->_renderTarget
		);
	}

	return hr;
}

void SpectrogramWindow::discardDeviceResourcesD2D()
{
	this->_renderTarget->Release();
	this->_renderTarget = nullptr;
}

LRESULT SpectrogramWindow::on_message(HWND const wnd, UINT const msg, WPARAM const wp, LPARAM const lp)
{
	switch (msg)
	{
		case WM_CREATE:
		{
			RECT rc;
			GetClientRect(wnd, &rc);

			this->_wnd = CreateWindowEx(0, WC_STATIC, _T("Spectrogram (CUI)"),
								  WS_CHILD | WS_VISIBLE, 0, 0, rc.right, rc.bottom,
								  wnd, HMENU(nullptr), core_api::get_my_instance(), nullptr);
			SetWindowLong(this->_wnd, GWL_WNDPROC, reinterpret_cast<LONG>(reinterpret_cast<WNDPROC>(onChildMessage)));
			this->createDeviceResourcesD2D();
			break;
		}
		case WM_SIZE:
		{
			RedrawWindow(this->_wnd, nullptr, nullptr, RDW_INVALIDATE | RDW_ERASE);
			SetWindowPos(this->_wnd, nullptr, 0, 0, LOWORD(lp), HIWORD(lp), SWP_NOZORDER);
			break;
		}
		case WM_DESTROY:
			this->discardDeviceResourcesD2D();
			this->_wnd = nullptr;
			break;
	}
	return DefWindowProc(wnd, msg, wp, lp);
}

LRESULT SpectrogramWindow::onChildMessage(HWND const wnd, UINT const msg, WPARAM const wp, LPARAM const lp)
{
	switch (msg)
	{
		case WM_PAINT:
		{
			for (auto& instance : _instances)
			{
				if (instance->_wnd != wnd)
					continue;
				instance->renderD2D();
				break;
			}
			break;
		}
		case WM_SIZE:
		{
			for (auto& instance : _instances)
			{
				if (instance->_wnd != wnd)
					continue;
				instance->resizeD2D(LOWORD(lp), HIWORD(lp));
				instance->renderD2D();
				break;
			}
			break;
		}
	}
	return DefWindowProc(wnd, msg, wp, lp);
}

SpectrogramWindow::class_data& SpectrogramWindow::get_class_data() const
{
	__implement_get_class_data(_T("{F1C52A2B-65AB-421B-AA69-ECC88016FB8C}"), true);
}

const GUID& SpectrogramWindow::get_extension_guid() const
{
	return _guid;
}

void SpectrogramWindow::get_name(pfc::string_base& out) const
{
	out.set_string("Spectrogram (CUI)");
}
void SpectrogramWindow::get_category(pfc::string_base& out) const
{
	out.set_string("Visualisations");
}
unsigned SpectrogramWindow::get_type() const
{
	return uie::type_panel;
}

void SpectrogramWindow::on_playback_starting(play_control::t_track_command, bool)
{
	startVisualisation();
}

void SpectrogramWindow::on_playback_new_track(metadb_handle_ptr)
{
	startVisualisation();
}

void SpectrogramWindow::on_playback_stop(play_control::t_stop_reason)
{
	stopVisualisation();
}

void SpectrogramWindow::on_playback_pause(bool const p_state)
{
	if (p_state)
		stopVisualisation();
	else
		startVisualisation();
}

HRESULT SpectrogramWindow::renderD2D()
{
	HRESULT hr = createDeviceResourcesD2D();

	if (SUCCEEDED(hr))
	{
		this->_renderTarget->BeginDraw();
		this->_renderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		this->_renderTarget->Clear(D2D1::ColorF(D2D1::ColorF::Black));
		D2D1_SIZE_F const rtSize = this->_renderTarget->GetSize();

		int i = 0;
		for (auto it = this->_lines.crbegin(); it != this->_lines.crend(); ++it)
		{
			this->_renderTarget->DrawBitmap(*it, D2D1::RectF(rtSize.width - i - 2, 0.f, rtSize.width - i, rtSize.height));
			i += 2;
		}

		hr = this->_renderTarget->EndDraw();
	}
	if (hr == D2DERR_RECREATE_TARGET)
	{
		hr = S_OK;
		discardDeviceResourcesD2D();
	}
	return hr;
}

void SpectrogramWindow::resizeD2D(UINT const width, UINT const height) const
{
	if (this->_renderTarget != nullptr)
		this->_renderTarget->Resize(D2D1::SizeU(width, height));
}

void SpectrogramWindow::startVisualisation()
{
	if (_timerPtr == 0)
	{
		_timerPtr = SetTimer(nullptr, 0x6694, 32, reinterpret_cast<TIMERPROC>(visualisationTimerProc));
		static_api_ptr_t<visualisation_manager>()->create_stream(_visualisationStream, visualisation_manager::KStreamFlagNewFFT);
	}
}

void SpectrogramWindow::stopVisualisation()
{
	if (_timerPtr != 0)
	{
		KillTimer(nullptr, _timerPtr);
		_timerPtr = 0;
		_visualisationStream.release();
	}
}

void SpectrogramWindow::onVisualisation(audio_chunk const& chunk)
{
	if (this->_renderTarget == nullptr)
		return;
	D2D1_SIZE_F const rtSize = this->_renderTarget->GetSize();
	audio_sample const* const data = chunk.get_data();
	t_size const sampleCount = chunk.get_sample_count(); // half of fft size, from 0 hz to sample rate / 2 hz
	t_size const channelCount = chunk.get_channel_count();
	double const minFreq = 33.0 / (chunk.get_sample_rate() / 2);
	double const log2SampleCountPlusOne = log2(sampleCount + 1.0);

	while (this->_lines.size() > static_cast<size_t>(rtSize.width) / 2)
	{
		this->_lines.front()->Release();
		this->_lines.pop_front();
	}

	UINT32* d = new UINT32[sampleCount];
	for (t_size i = 0; i < sampleCount; ++i)
	{
		double floatingIndex = lerp(minFreq, 1.0, pow(sampleCount, (sampleCount - 1.0 - i) / sampleCount - 1.0)) * sampleCount;
		int index = static_cast<int>(floor(floatingIndex));
		double sample = 0;
		for (t_size j = 0; j < channelCount; ++j)
			sample += data[index * channelCount + j];
		sample = log2(sample * sampleCount + 1.0) / log2SampleCountPlusOne;
		if (sample > 1.0)
			sample = 1.0;
		floatingIndex -= index;

		double neighbourSample = 0.0;
		if (floatingIndex > 0.5)
		{
			index += 1;
			if (index < sampleCount)
				for (t_size j = 0; j < channelCount; ++j)
					neighbourSample += data[index * channelCount + j];
			floatingIndex = (floatingIndex - 0.5) * 2.0;
		}
		else
		{
			index -= 1;
			if (index >= 0)
				for (t_size j = 0; j < channelCount; ++j)
					neighbourSample += data[index * channelCount + j];
			floatingIndex = (0.5 - floatingIndex) * 2.0;
		}
		neighbourSample = log2(neighbourSample * sampleCount + 1.0) / log2SampleCountPlusOne;
		if (neighbourSample > 1.0)
			neighbourSample = 1.0;
		sample = lerp(sample, (sample + neighbourSample) / 2.0, floatingIndex);

		byte const b = static_cast<byte>(sample * 255);
		d[i] = ((255 << 24) | (b << 16) | (b << 8) | b);
	}

	ID2D1Bitmap* bitmap;
	this->_renderTarget->CreateBitmap(
		D2D1::SizeU(1, static_cast<UINT32>(sampleCount)),
		d,
		4,
		D2D1::BitmapProperties(
			D2D1::PixelFormat(
				DXGI_FORMAT_B8G8R8A8_UNORM,
				D2D1_ALPHA_MODE_IGNORE
			)
		),
		&bitmap);

	this->_lines.push_back(bitmap);

	delete[](d);

	InvalidateRect(this->_wnd, nullptr, TRUE);
}

void SpectrogramWindow::visualisationTimerProc(HWND const wnd, UINT const msg, UINT const event, DWORD const time)
{
	double t;
	_visualisationStream->get_absolute_time(t);
	audio_chunk_impl chunk;
	if (_instances.empty() == false && _visualisationStream->get_spectrum_absolute(chunk, t, 8192))
	{
		for (auto& instance : _instances)
			instance->onVisualisation(chunk);
	}
}

inline double SpectrogramWindow::lerp(double const d1, double const d2, double const factor)
{
	return d1 * (1.0 - factor) + d2 * factor;
}

const GUID SpectrogramWindow::_guid = {0xF1C52A2B, 0x65AB, 0x421B, { 0xAA, 0x69, 0xEC, 0xC8, 0x80, 0x16, 0xFB, 0x8C }};
UINT_PTR SpectrogramWindow::_timerPtr = 0;
service_ptr_t<visualisation_stream> SpectrogramWindow::_visualisationStream = nullptr;
std::list<SpectrogramWindow*> SpectrogramWindow::_instances;
ID2D1Factory* SpectrogramWindow::_direct2dFactory = nullptr;

uie::window_factory<SpectrogramWindow> gSpectrogramWindowFactory;